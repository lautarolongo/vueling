﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Entities;
using System.Text;
using Newtonsoft.Json;
using VuelingSolution.DTO.Request;
using VuelingSolution.Util;
using Microsoft.AspNetCore.Hosting;
using System.IO;
using VuelingSolution.DTO.Response;
using VuelingSolution.Services.Interfaces;
using VuelingSolution.Models;
using VuelingSolution.ExceptionHandlers;
using LoggerService;

namespace VuelingSolution.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class VuelingController : Controller
    {
        private readonly ILoggerManager _logger;
        private readonly IExchangeRateService _exchangeRateService;
        private readonly ITransaccionService _transaccionService;
        public VuelingController(IExchangeRateService exchangeRateService, ITransaccionService transaccionService, ILoggerManager logger)
        {
            _exchangeRateService = exchangeRateService;
            _transaccionService = transaccionService;
            _logger = logger;
        }


        [HttpGet("rates/all")]
        public async Task<IActionResult> GetAllRates()
        {
            Wrapper<List<ExchangeRate>> response;
            try
            {
                List<ExchangeRate> exchangeRates = await _exchangeRateService.GetAllExchangeRates();
                response = new Wrapper<List<ExchangeRate>>(exchangeRates);
                _logger.LogInfo("Consulta de Rates");
                return Ok(response);
            }
            catch (HttpRequestException ex)
            {
               _logger.LogError(ex.ToString());
                response = new Wrapper<List<ExchangeRate>>();
                response.Success = false;
                response.Message = "Ocurrio un error en la consulta de ExchangeRates";
                return BadRequest(response);
            }

            
        }


        [HttpGet("transactions/all")]
        public async Task<IActionResult> GetAllTransactions()
        {
            Wrapper<List<Transaccion>> response;
            try
            {
                List<Transaccion> Transacciones = await _transaccionService.GetAll();
                response = new Wrapper<List<Transaccion>>(Transacciones);
                _logger.LogInfo("Consulta de Transactions");
                return Ok(response);
            }
            catch (HttpRequestException ex)
            {
                _logger.LogError(ex.ToString());
                response = new Wrapper<List<Transaccion>>();
                response.Success = false;
                response.Message = "Ocurrio un error en la consulta de Transacciones";
                return BadRequest(response);
            }

           
        }

        [HttpPost("transactions/bySku")]
        public async Task<IActionResult> GetTransactions([FromBody] TransactionRequestDTO transactionDTO)
        {
            Wrapper<TransactionResponseDTO> response;
            try
            {
                if (!string.IsNullOrEmpty(transactionDTO.Sku) && transactionDTO.Sku.Length == 5)
                {
                    response = new Wrapper<TransactionResponseDTO>(await _transaccionService.GetBySku(transactionDTO.Sku));
                    _logger.LogInfo("Consulta SKU");
                }
                else
                {
                    _logger.LogInfo("SKU Incorrecto");
                    throw new InvalidTransactionException("Invalid SKU Id");
                }

                return Ok(response);

            }
            catch (HttpRequestException ex)
            {
                _logger.LogError(ex.ToString());
                response = new Wrapper<TransactionResponseDTO>();
                response.Success = false;
                response.Message = "Ocurrio un error en la consulta de Transacciones bySku";
                return BadRequest(response);
            }
        }
    }
}