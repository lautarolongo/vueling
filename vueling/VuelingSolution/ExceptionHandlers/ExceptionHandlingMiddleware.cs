﻿using LoggerService;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using VuelingSolution.Models;

namespace VuelingSolution.ExceptionHandlers
{
    public class ExceptionHandlingMiddleware
    {
        public RequestDelegate requestDelegate;
        private  readonly ILoggerManager _logger;
        private InvalidTransactionException InvalidTransactionException;

        public ExceptionHandlingMiddleware(RequestDelegate requestDelegate, ILoggerManager logger)
        {
            this.requestDelegate = requestDelegate;
            this._logger = logger;
        }
        public async Task Invoke(HttpContext context)
        {
            try
            {
                await requestDelegate(context);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                await HandleException(context, ex);
            }
           
        }
        private static Task HandleException(HttpContext context, Exception ex)
        {
            var errorMessageObject = new Error { Message = ex.Message, Code = "GE" };
            var statusCode = (int)HttpStatusCode.InternalServerError;
            switch (ex)
            {
                //case InvalidTransactionException:
                //    errorMessageObject.Code = "M001";
                //    statusCode = (int)HttpStatusCode.BadRequest;
                //    break;

            }

            Wrapper<Error> errorWrapper = new Wrapper<Error>();
            errorWrapper.Success = false;
            errorWrapper.Data = errorMessageObject;
            errorWrapper.Error = "true";
            
            var errorMessage = JsonConvert.SerializeObject(errorWrapper);
            

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;

            return context.Response.WriteAsync(errorMessage);
        }

    }
}
