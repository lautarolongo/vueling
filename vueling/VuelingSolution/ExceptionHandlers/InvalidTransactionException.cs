﻿using System;

namespace VuelingSolution.ExceptionHandlers
{
    public class InvalidTransactionException : Exception
    {
        public InvalidTransactionException(string message) : base(message)
        {
        }
    }
}
