﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VuelingSolution.Models
{

    public class Wrapper<T>
    {
        public Boolean Success { get; set; }
        public T Data { get; set; }
        public string Message { get; set; }
        public string Error { get; set; }

        public Wrapper()
        {
        }

        public Wrapper(T data)
        {
            this.Data = data;
            this.Success = true;
        }

    }
}

