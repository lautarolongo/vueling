﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VuelingSolution.Services.Interfaces
{
    public interface IExchangeRateService
    {
        public Task<List<ExchangeRate>> GetAllExchangeRates();
    }
        
}
