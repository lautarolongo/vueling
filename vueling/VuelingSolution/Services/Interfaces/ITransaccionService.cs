﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using VuelingSolution.DTO.Response;

namespace VuelingSolution.Services.Interfaces
{
    public interface ITransaccionService
    {
        public Task<List<Transaccion>> GetAll();
        public Task<TransactionResponseDTO> GetBySku(string value);
    }
}
