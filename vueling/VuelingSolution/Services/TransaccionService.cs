﻿using Entities;
using LoggerService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using VuelingSolution.DTO.Response;
using VuelingSolution.Services.Interfaces;
using VuelingSolution.Util;

namespace VuelingSolution.Services
{
    public class TransaccionService : ITransaccionService
    {
        private HttpClient _httpClient;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private IConfiguration _configuration;
        private readonly ILoggerManager _logger;

        public TransaccionService(HttpClient httpClient, IConfiguration configuration, IWebHostEnvironment webHostEnvironment, ILoggerManager logger)
        {
            _httpClient = httpClient;
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
            _logger = logger;
        }

        public async Task<List<Transaccion>> GetAll()
        {

            List<Transaccion> transactions;
            try
            {

                var uri = _configuration.GetValue<string>("ApiEndpoints:Transactions");

                var result = await _httpClient.GetAsync(uri);
                if (result.IsSuccessStatusCode)
                {
                    var getResponsestring = await result.Content.ReadAsStringAsync();
                    //Persisto Archivo Transaccion.Json
                    string transactionsFileName = _configuration.GetValue<string>("Files:Transactions");
                    FileWrapper.SaveFile(Path.Combine(_webHostEnvironment.ContentRootPath, "Files", transactionsFileName), getResponsestring);
                    transactions = JsonConvert.DeserializeObject<List<Transaccion>>(getResponsestring);

                }
                else
                {
                    //Si el endpoint no funciona recupero la data desde el archivo 
                    var filename = _configuration.GetValue<string>("Files:Transactions");
                    string transactionsFile = FileWrapper.ReadFile(Path.Combine(_webHostEnvironment.ContentRootPath, "Files", filename));
                    transactions =
                            JsonConvert.DeserializeObject<List<Transaccion>>(transactionsFile);
                }

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                transactions = new List<Transaccion>();
            }

            return transactions;
        }

        public async Task<TransactionResponseDTO> GetBySku(string sku)
        {
            TransactionResponseDTO transactionResponseDTO = new TransactionResponseDTO();
            try
            {

                //var uri = _configuration.GetValue<string>("ApiEndpoints:Transactions");
                //var result = await _httpClient.GetAsync(uri);
                string transactionsFileName;
                List<Transaccion> transactions;
                //if (result.IsSuccessStatusCode)
                //{
                //    var getResponsestring = await result.Content.ReadAsStringAsync();
                //    //Persisto Archivo Transaccion.Json
                //    transactionsFileName = _configuration.GetValue<string>("Files:Transactions");
                //    FileWrapper.SaveFile(Path.Combine(_webHostEnvironment.ContentRootPath, "Files", transactionsFileName), getResponsestring);
                //    transactions = JsonConvert.DeserializeObject<List<Transaccion>>(getResponsestring);
                //}
                //else
                //{
                    transactionsFileName = _configuration.GetValue<string>("Files:Transactions");
                    string transactionsFile = FileWrapper.ReadFile(Path.Combine(_webHostEnvironment.ContentRootPath, "Files", transactionsFileName));
                    transactions =
                            JsonConvert.DeserializeObject<List<Transaccion>>(transactionsFile);
                //}

                string exchangeRatesTableFileName = _configuration.GetValue<string>("Files:ExchangeRatesTable");
                string exchangeConvertionsFile = FileWrapper.ReadFile(Path.Combine(_webHostEnvironment.ContentRootPath, "Files", exchangeRatesTableFileName));
                List<ExchangeRate> exchangeConvertions =
                        JsonConvert.DeserializeObject<List<ExchangeRate>>(exchangeConvertionsFile);

                var query = transactions.Where(p => p.Sku.ToUpper() == sku.ToUpper()).ToList();
                string moneda = _configuration.GetValue<string>("Moneda");
                if (query.Count() > 0)
                {

                    decimal total = 0;
                    foreach (Transaccion t in query)
                    {
                        
                        if (t.Currency.ToUpper() == moneda)
                        {
                            total += t.Amount;
                        }
                        else
                        {
                            //Convertir a EUR  y redondeamos e resultado en  Banker’s Rounding
                            t.Amount = decimal.Round(t.Amount * exchangeConvertions.First(x => x.From == t.Currency && x.To == moneda).Rate, 2, MidpointRounding.ToEven);
                            t.Currency = moneda;
                        }
                    }

                    transactionResponseDTO.transacciones = query;
                    transactionResponseDTO.total += decimal.Round(total);
                    transactionResponseDTO.message = "Total Expresados en EUROS y redondeados a 2 decimales";
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
            }

            return transactionResponseDTO;
        }
    }
}
