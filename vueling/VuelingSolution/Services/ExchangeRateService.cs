﻿using Entities;
using LoggerService;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using VuelingSolution.Services.Interfaces;
using VuelingSolution.Util;

namespace VuelingSolution.Services
{
    public class ExchangeRateService : IExchangeRateService
    {

        private HttpClient _httpClient;
        private readonly IWebHostEnvironment _webHostEnvironment;
        private IConfiguration _configuration;
        private readonly ILoggerManager _logger;

        public ExchangeRateService(HttpClient httpClient, IConfiguration configuration, IWebHostEnvironment webHostEnvironment, ILoggerManager logger)
        {
            _httpClient = httpClient;
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
            _logger = logger;

        }

        public async Task<List<ExchangeRate>> GetAllExchangeRates()
        {
            List<ExchangeRate> exchangeRates;
            try
            {
                var uri = _configuration.GetValue<string>("ApiEndpoints:Rates");
                var result = await _httpClient.GetStringAsync(uri);
                exchangeRates = JsonConvert.DeserializeObject<List<ExchangeRate>>(result);
                //Persisto Archivo Rates.Json
                string ratesFileName = _configuration.GetValue<string>("Files:Rates");
                FileWrapper.SaveFile(Path.Combine(_webHostEnvironment.ContentRootPath, "Files", ratesFileName), result);

                //Creo tabla de conversiones , para calcular las conversiones de todas las monedas a euro
                var tablaConversiones = ConvertRateWrapper.ConvertToCoin(exchangeRates);

                var json = JsonConvert.SerializeObject(tablaConversiones);
                //Persisto Archivo ExchangeRatesConvertion
                string exchangeRatesTableFileName = _configuration.GetValue<string>("Files:ExchangeRatesTable");
                FileWrapper.SaveFile(Path.Combine(_webHostEnvironment.ContentRootPath, "Files", exchangeRatesTableFileName), json);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.ToString());
                exchangeRates = new List<ExchangeRate>();
            }

            return exchangeRates;

        }


    }
}
