﻿using Entities;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace VuelingSolution.Util
{
    public class ConvertRateWrapper
    {
        private IConfiguration _configuration;

        public ConvertRateWrapper(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public static List<ExchangeRate> ConvertToCoin(List<ExchangeRate> exchangeRates)
        {
            Dictionary<string, object> Monedas = new Dictionary<string, object>();
            Dictionary<string, int> md = new Dictionary<string, int>();
            Dictionary<string, object> res = new Dictionary<string, object>();

            //calculo la cantidad de diferentes monedas
            var aux = exchangeRates
                .GroupBy(i => i.From)
                .Select(g => g.Key).Distinct().ToList();

            int i = 0;
            foreach (string exr in aux)
            {
                md.Add(exr, i);
                i++;
            }

            decimal[,] graph = new decimal[i, i];
            for (int j = 0; j < i; j++)
                for (int k = 0; k < j; k++)
                    graph[j, k] = 0;

            //Armo matriz de cotizaciones
            foreach (ExchangeRate exr in exchangeRates)
            {
                graph[md[exr.From], md[exr.To]] = exr.Rate;
            }

            List<ExchangeRate> resultado = new List<ExchangeRate>();
            foreach (string moneda in aux)
            {
                //calculo distancias de todas las monedas
                decimal[] re = dijkstra(graph, md[moneda], i);
                
                ExchangeRate exchangeRate;
                for (int p = 0; p < re.Count(); p++)
                {
                    exchangeRate = new ExchangeRate();
                    exchangeRate.From = moneda;
                    exchangeRate.To = aux[p];
                    exchangeRate.Rate = re[p];
                    resultado.Add(exchangeRate);
                }

                //res.Add(moneda, resultado);
            }

            //foreach (string exr in aux)
            //{

            //    if(pos != positionOfCoin)
            //    {
            //        res.Add(exr, dijkstra(graph, md[exr], i));
            //        exchangeRate = new ExchangeRate();
            //        exchangeRate.From = exr;
            //        exchangeRate.To = coin;
            //        exchangeRate.Rate = dijkstra(graph, md[exr], i);
            //        resultado.Add(exchangeRate);
            //    }
            //    pos++;
            //}

            return resultado;

            //res.Add("CAD", dijkstra(graph, md["CAD"], i));
            //res.Add("AUD", dijkstra(graph, md["AUD"], i));
            //res.Add("USD", dijkstra(graph, md["USD"], i));

        }

        private static decimal[] dijkstra(decimal[,] graph, int src, int V)
        {
            decimal[] dist = new decimal[V]; // The output array. dist[i]
                                             // will hold the shortest
                                             // distance from src to i

            // sptSet[i] will true if vertex
            // i is included in shortest path
            // tree or shortest distance from
            // src to i is finalized
            bool[] sptSet = new bool[V];

            // Initialize all distances as
            // INFINITE and stpSet[] as false
            for (int i = 0; i < V; i++)
            {
                dist[i] = int.MaxValue;
                sptSet[i] = false;
            }

            // Distance of source vertex
            // from itself is always 0
            dist[src] = 0;

            // Find shortest path for all vertices
            for (int count = 0; count < V - 1; count++)
            {
                // Pick the minimum distance vertex
                // from the set of vertices not yet
                // processed. u is always equal to
                // src in first iteration.
                int u = minDistance(dist, sptSet, V);

                // Mark the picked vertex as processed
                sptSet[u] = true;

                // Update dist value of the adjacent
                // vertices of the picked vertex.
                for (int v = 0; v < V; v++)

                    // Update dist[v] only if is not in
                    // sptSet, there is an edge from u
                    // to v, and total weight of path
                    // from src to v through u is smaller
                    // than current value of dist[v]
                    if (!sptSet[v] && graph[u, v] != 0 &&
                         dist[u] != int.MaxValue && dist[u] + graph[u, v] < dist[v])
                        dist[v] = dist[u] + graph[u, v];
            }

            // print the constructed distance array
            for (int i = 0; i < V; i++)
            {
                Console.Write(i + " \t\t " + dist[i] + "\n");
            }

            return dist;
            //printSolution(dist, V);
        }

        private static int minDistance(decimal[] dist,
                       bool[] sptSet, int V)
        {
            // Initialize min value
            decimal min = int.MaxValue;
            int min_index = -1;

            for (int v = 0; v < V; v++)
                if (sptSet[v] == false && dist[v] <= min)
                {
                    min = dist[v];
                    min_index = v;
                }

            return min_index;
        }

    }
}
