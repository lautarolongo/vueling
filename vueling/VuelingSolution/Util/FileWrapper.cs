﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace VuelingSolution.Util
{
    public static class FileWrapper
    {

        public static void SaveFile(string fileName,string data)
        {
            try {

                
                
                if (File.Exists(fileName))
                    File.Delete(fileName);

                using (var tw = new StreamWriter(fileName, true))
                {
                    tw.WriteLine(data);
                    tw.Close();
                }
            } 
            catch (Exception ex)
            {

            }
        }

        public static string ReadFile(string fileName)
        {
            try
            {
                
                string strText = "";

                using (StreamReader r = new StreamReader(fileName))
                {

                    strText = r.ReadToEnd();
                    //dynamic array = JsonConvert.DeserializeObject(json);

                }

                return strText;

            }
            catch (Exception ex)
            {

            }

            return "";
        }

    }
}
