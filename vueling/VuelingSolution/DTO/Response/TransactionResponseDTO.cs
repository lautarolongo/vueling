﻿using Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace VuelingSolution.DTO.Response
{
    public class TransactionResponseDTO
    {
        public List<Transaccion> transacciones { get; set; }

        public decimal total { get; set; }
        public string message { get; set; }
    }
}
