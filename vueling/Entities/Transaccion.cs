﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class Transaccion
    {
        public string Sku { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }

        public Transaccion() { }
    }
}
