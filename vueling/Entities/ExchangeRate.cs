﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace Entities
{
    public class ExchangeRate
    {
        [JsonProperty("from")]
        public string From { get; set; }
        [JsonProperty("to")]
        public string To { get; set; }
        [JsonProperty("rate")]
        public decimal Rate { get; set; }

        public ExchangeRate() { }

    }
}
