﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    public class SkuException : Exception
    {
        public override string Message => "Sku es incorrecto, debe contener 5 caracteres";
    }
}
