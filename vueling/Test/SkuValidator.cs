﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Test
{
    public class SkuValidator
    {
        public bool isValidSku(string sku)
        {
            if (string.IsNullOrEmpty(sku) || sku.Length != 5)
                throw new SkuException();

            return true;
        }
    }
}
