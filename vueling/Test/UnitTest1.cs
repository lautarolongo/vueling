using Entities;
using LoggerService;
using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VuelingSolution.Controllers;
using VuelingSolution.Services.Interfaces;
using Xunit;

namespace Test
{
    public class UnitTest1
    {
        [Fact]
        public void IncorrectSku()
        {
            var skuValidator = new SkuValidator();
            string sku_test1 = "AA";

            bool invalidSku = skuValidator.isValidSku(sku_test1);
            Assert.True(invalidSku);
        }

        [Fact]
        public void CorrectSku()
        {
            var skuValidator = new SkuValidator();
            string sku_test2 = "ABCD5";


            bool correctSku = skuValidator.isValidSku(sku_test2);

            Assert.True(correctSku);


        }

        [Fact]
        public void TransactionFakeSkuTest()
        {
            var ELoggerMock = new Mock<ILoggerManager>();
            var EITransaccionServiceMock = new Mock<ITransaccionService>();

            EITransaccionServiceMock.Setup(arg => arg.GetAll()).Throws(new InvalidOperationException());

        }


    }
}
